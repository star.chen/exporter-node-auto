#!/bin/bash -e
YUM_CMD=$(which yum);
APT_GET_CMD=$(which apt-get);



if [[ ! -z $YUM_CMD ]]; then
   sudo yum install -y git wget
   echo "Centos系統安裝git wget"
elif [[ ! -z $APT_GET_CMD ]]; then
   sudo apt-get git wget -y
   echo "Ununtu系統安裝git wget"
fi



#安裝docker及docker-compose
if [[ $(which docker) && $(docker --version) ]]; then
   echo "docker已經安裝"
else
   echo "docker安裝中"
   if [[ ! -z $YUM_CMD ]]; then
      sudo yum install -y docker
      echo "Centos系統安裝docker"
   elif [[ ! -z $APT_GET_CMD ]]; then
      sudo apt-get -y docker
      echo "Ununtu系統安裝docker"
   fi
   systemctl enable docker
   systemctl start docker
fi
if [[ $(which docker-compose) && $(docker-compose verison) ]]; then
   echo "docker-compose已經安裝"
else
   echo "docker-compose安裝中"
   sudo curl -L "https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
   sudo chmod +x /usr/local/bin/docker-compose
   sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
fi



#從gitlab下載docker-installation
cd /opt
if [ -d "/opt/docker-installation" ]; then
   sudo rm -rf /opt/docker-installation
   git clone https://gitlab.com/star.chen/docker-installation.git
else
   git clone https://gitlab.com/star.chen/docker-installation.git
fi

#以下不開啟
#cd docker-installation
#sh install_docker.sh
#systemctl enable docker



#從gitlab下載prometheus-some-exporters
cd /opt
if [ -d "/opt/prometheus-some-exporters" ]; then
   sudo rm -rf /opt/prometheus-some-exporters
   git clone https://gitlab.com/star.chen/prometheus-some-exporters.git
else
   git clone https://gitlab.com/star.chen/prometheus-some-exporters.git
fi
cd prometheus-some-exporters
ip=$(ip add | grep -w  "BROADCAST" | awk -F":" 'NR==1{print $2}' | sed s/[[:space:]]//g)
echo "网卡名称为:" $ip
ifcfg=$(find /etc -name *$ip)
echo "网卡路径为:" $ifcfg
if [[ "$ip" != "eth0" ]];then
echo "更改init_caddyfile.sh裡面的網卡名稱為:" $ip
sed -e 's/eth0/'"$ip"'/g' -i /opt/prometheus-some-exporters/init_caddyfile.sh
fi
sh load_docker_image.sh
cd exporter
docker-compose up -d
